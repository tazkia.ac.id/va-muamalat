package id.ac.tazkia.payment.muamalat.va.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@SpringBootTest
public class ApiKeyConfigurationTests {
    @Value("classpath:/json/create-va-request.json") private Resource sampleCreateVaRequest;

    @Autowired private ApiKeyConfiguration apiKeyConfiguration;

    @Test
    public void testGenerateSignature() throws Exception {
        String request = new BufferedReader(
                new InputStreamReader(sampleCreateVaRequest.getInputStream()))
                .lines().collect(Collectors.joining());
        String signature = apiKeyConfiguration.calculateSignature(request);
        System.out.println("Signature : "+signature);
        Assertions.assertNotNull(signature);
        Assertions.assertEquals("9cbe50d52f973df32a798bc9af05975c1983d7963427e8735ccfcbcae74c9bf4",
                signature);
    }
}
