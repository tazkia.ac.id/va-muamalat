package id.ac.tazkia.payment.muamalat.va.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.payment.muamalat.va.dto.PaymentNotificationRequest;
import id.ac.tazkia.payment.muamalat.va.entity.VirtualAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import reactor.test.StepVerifier;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.stream.Collectors;

@SpringBootTest
public class BankMuamalatVaClientServiceTests {
    @Autowired private BankMuamalatVaClientService service;
    @Autowired private ObjectMapper objectMapper;
    @Value("classpath:/json/payment-notification-request.json") private Resource paymentNotificationJson;

    @Test
    public void testCreateVa() {
        service.createVa(createSampleVa())
                .map(r -> {
                    System.out.println(r);
                    return r;
                }).as(StepVerifier::create)
                .expectNextMatches(r -> r.getId() != null)
                .verifyComplete();
    }

    @Test
    public void testGetAllVa() {
        service.getAllVirtualAccounts()
                .map(r -> {
                    System.out.println(r);
                    return r;
                }).as(StepVerifier::create)
                .expectNextMatches(response ->
                    "success".equalsIgnoreCase(response.getStatus())
                ).verifyComplete();
    }

    @Test
    public void testParsePaymentNotification() throws Exception {
        String strPayment = new BufferedReader(new InputStreamReader(paymentNotificationJson.getInputStream())).lines().collect(Collectors.joining());
        Assertions.assertNotNull(strPayment);
        PaymentNotificationRequest request = objectMapper.readValue(strPayment, PaymentNotificationRequest.class);
        Assertions.assertNotNull(request.getTrx_date());
        System.out.println("Trx date : "+request.getTrx_date());
    }

    public static VirtualAccount createSampleVa() {
        VirtualAccount va = new VirtualAccount();
        va.setAccountNumber("1234567890");
        va.setAccountName("Test Customer");
        va.setInvoiceType("Product 001");
        va.setInvoiceNumber("0987654321");
        va.setAmount(new BigDecimal(10000000));
        return va;
    }
}
