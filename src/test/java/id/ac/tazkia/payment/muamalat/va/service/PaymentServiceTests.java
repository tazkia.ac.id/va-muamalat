package id.ac.tazkia.payment.muamalat.va.service;

import id.ac.tazkia.payment.muamalat.va.dao.VirtualAccountDao;
import id.ac.tazkia.payment.muamalat.va.entity.AccountStatus;
import id.ac.tazkia.payment.muamalat.va.entity.PaymentStatus;
import id.ac.tazkia.payment.muamalat.va.entity.VirtualAccount;
import id.ac.tazkia.payment.muamalat.va.exception.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.util.StringUtils;
import reactor.test.StepVerifier;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

@SpringBootTest
public class PaymentServiceTests {

    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PaymentService paymentService;
    @Autowired private DatabaseClient databaseClient;
    @Value("classpath:/sql/reset-data.sql") private Resource resetDatabaseScript;
    @Value("classpath:/sql/sample-data.sql") private Resource sampleDataScript;

    @BeforeEach
    public void resetDatabase() throws Exception {
        new BufferedReader(new InputStreamReader(resetDatabaseScript.getInputStream()))
                .lines().forEach(sql -> {
            if(StringUtils.hasText(sql)) {
                databaseClient.sql(sql).then().block();
            }
        });

        new BufferedReader(new InputStreamReader(sampleDataScript.getInputStream()))
                .lines().forEach(sql -> {
                    if(StringUtils.hasText(sql)) {
                        databaseClient.sql(sql).then().block();
                    }
                });
    }

    @Test
    public void testFindAccountByNumber() {
        System.out.println("Test find account by number");
        String accountNumber = "081234567890";
        paymentService.findByAccountNumber(accountNumber)
                .map(va -> {
                    System.out.println(va);
                    return va;
                })
                .as(StepVerifier::create)
                .expectNextMatches(va -> va.getAccountNumber().equalsIgnoreCase(accountNumber))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void testCreateVaSuccess() {
        String accountNumber = "081234567777";
        VirtualAccount va = createSampleVa(accountNumber);
        paymentService.create(va)
                .map(v -> {
                    System.out.println("VA id : " + v.getId());
                    return v;
                })
                .as(StepVerifier::create)
                .expectNextMatches(v -> v.getId() != null)
                .verifyComplete();
    }

    @Test
    public void testCreateVaNumberAlreadyExists() {
        String accountNumber = "081234567890";

        virtualAccountDao.findByAccountNumberAndAccountStatus(accountNumber, AccountStatus.ACTIVE)
                .as(StepVerifier::create)
                .expectNextCount(1);

        VirtualAccount va = createSampleVa(accountNumber);
        paymentService.create(va)
                .as(StepVerifier::create)
                .verifyError(VirtualAccountNumberAlreadyExistsException.class);
    }

    @Test
    public void testCreateInvoiceNumberAlreadyExists() {
        String accountNumber = "99999999999";
        String invoiceNumber = "20180820002";

        virtualAccountDao.findByInvoiceNumberAndAccountStatus(invoiceNumber, AccountStatus.ACTIVE)
                .as(StepVerifier::create)
                .expectNextCount(1);

        VirtualAccount va = createSampleVa(accountNumber);
        va.setInvoiceNumber(invoiceNumber);
        paymentService.create(va)
                .as(StepVerifier::create)
                .verifyError(InvoiceNumberAlreadyExistsException.class);
    }

    @Test
    public void testUpdateVaNormal() {

        paymentService.findByAccountNumber("081234567890")
                .last()
                .flatMap(va -> {
                    va.setAmount(new BigDecimal(123001));
                    return paymentService.update(va);
                })
                .as(StepVerifier::create)
                .expectNextMatches(v -> v.getAmount().compareTo(new BigDecimal(123001)) == 0)
                .verifyComplete();


        paymentService.findByAccountNumber("081234567890")
                .as(StepVerifier::create)
                .expectNextMatches(v -> v.getAmount().compareTo(new BigDecimal(123001)) == 0)
                .verifyComplete();
    }

    @Test
    public void testUpdateAlreadyHasPayment() {
        paymentService.findByAccountNumber("081234567897")
                .last()
                .flatMap(va -> {
                    va.setAmount(new BigDecimal(123001));
                    return paymentService.update(va);
                })
                .as(StepVerifier::create)
                .verifyError(VirtualAccountAlreadyPaidException.class);
    }

    @Test
    public void testDeleteVa() {
        paymentService.delete("20180820005")
                .as(StepVerifier::create)
                .verifyComplete();
    }

    @Test
    public void testDeleteVaNotExists() {
        paymentService.delete("20180820")
                .as(StepVerifier::create)
                .verifyError(VirtualAccountNotFoundException.class);
    }

    @Test
    public void testDeletePaidVa() {
        paymentService.delete("20180820008")
                .as(StepVerifier::create)
                .verifyError(VirtualAccountAlreadyPaidException.class);
    }

    @Test
    public void testPaymentSuccess() {
        String invoiceNumber = "20180820001";
        BigDecimal amount = new BigDecimal(123000);
        String referenceNumber = "abcd-abcd";

        paymentService.pay(invoiceNumber, amount, referenceNumber)
                .as(StepVerifier::create)
                .expectNextMatches(p ->
                    p.getId() != null
                        && amount.compareTo(p.getAmount()) == 0
                        && PaymentStatus.FULL.equals(p.getPaymentStatus())
                        && AccountStatus.INACTIVE.equals(p.getVirtualAccount().getAccountStatus())
                        && PaymentStatus.FULL.equals(p.getVirtualAccount().getPaymentStatus())
                ).verifyComplete();

        virtualAccountDao.findByInvoiceNumberAndAccountStatus(invoiceNumber, AccountStatus.INACTIVE)
                .as(StepVerifier::create)
                .expectNextMatches(va ->
                    AccountStatus.INACTIVE.equals(va.getAccountStatus())
                        && PaymentStatus.FULL.equals(va.getPaymentStatus())
                ).verifyComplete();
    }

    @Test
    public void testPaymentVaNotExists() {
        String invoiceNumber = "981234567890";
        BigDecimal amount = new BigDecimal(123000);
        String referenceNumber = "abcd-abcd";

        paymentService.pay(invoiceNumber, amount, referenceNumber)
                .as(StepVerifier::create)
                .verifyError(VirtualAccountNotFoundException.class);
    }

    @Test
    public void testPaymentAmountMismatch() {
        String invoiceNumber = "20180820001";
        BigDecimal amount = new BigDecimal(123001);
        String referenceNumber = "abcd-abcd";

        paymentService.pay(invoiceNumber, amount, referenceNumber)
                .as(StepVerifier::create)
                .verifyError(PaymentAmountMismatchException.class);

        paymentService.pay("20180820003", new BigDecimal(9000), referenceNumber)
                .as(StepVerifier::create)
                .verifyError(PaymentAmountMismatchException.class);

        paymentService.pay("20180820004", new BigDecimal(11000000), referenceNumber)
                .as(StepVerifier::create)
                .verifyError(PaymentAmountMismatchException.class);
    }

    private VirtualAccount createSampleVa(String accountNumber) {
        VirtualAccount va = new VirtualAccount();
        va.setAccountNumber(accountNumber);
        va.setInvoiceNumber("0987654321");
        va.setAccountName("Test Account 001");
        va.setDescription("Test VA 001");
        va.setEmail("va01@yopmail.com");
        va.setInvoiceType("Test Invoice");
        va.setPhone(accountNumber);
        va.setAmount(new BigDecimal(123000));
        return va;
    }
}
