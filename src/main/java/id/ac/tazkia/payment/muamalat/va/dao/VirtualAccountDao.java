package id.ac.tazkia.payment.muamalat.va.dao;

import id.ac.tazkia.payment.muamalat.va.entity.AccountStatus;
import id.ac.tazkia.payment.muamalat.va.entity.VirtualAccount;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface VirtualAccountDao extends ReactiveCrudRepository<VirtualAccount, String> {
    Flux<VirtualAccount> findByAccountNumberAndAccountStatus(String accountNumber, AccountStatus accountStatus);
    Mono<VirtualAccount> findByInvoiceNumber(String invoiceNumber);
    Mono<VirtualAccount> findByInvoiceNumberAndAccountStatus(String invoiceNumber, AccountStatus active);
}
