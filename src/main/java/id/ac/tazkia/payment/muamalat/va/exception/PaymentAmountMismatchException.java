package id.ac.tazkia.payment.muamalat.va.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PaymentAmountMismatchException extends PaymentServiceException {
    public PaymentAmountMismatchException(String message){
        super(message);
    }
}