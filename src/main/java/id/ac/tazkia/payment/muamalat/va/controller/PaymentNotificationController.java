package id.ac.tazkia.payment.muamalat.va.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.payment.muamalat.va.config.ApiKeyConfiguration;
import id.ac.tazkia.payment.muamalat.va.dto.PaymentNotificationRequest;
import id.ac.tazkia.payment.muamalat.va.exception.VirtualAccountNotFoundException;
import id.ac.tazkia.payment.muamalat.va.service.KafkaSenderService;
import id.ac.tazkia.payment.muamalat.va.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController @Slf4j
public class PaymentNotificationController {

    @Autowired private ObjectMapper objectMapper;
    @Autowired private PaymentService paymentService;
    @Autowired private KafkaSenderService kafkaSenderService;
    @Autowired private ApiKeyConfiguration apiKeyConfiguration;

    @PostMapping("/payment")
    public Mono<ResponseEntity<Object>> handlePaymentNotification(
            @RequestHeader("X-CLASSID-SIGNATURE") String signature,
            @RequestBody @Valid String request) {

        try {
            if (!isValid(signature, request)) {
                return Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
            }
        } catch (Exception err) {
            log.error("Error processing payment notification : {} | {}", err.getMessage(), request);
            return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        }

        try {
            PaymentNotificationRequest paymentNotificationRequest
                    = objectMapper.readValue(request, PaymentNotificationRequest.class);
            log.info("Receive payment notification for invoice {}",
                    paymentNotificationRequest.getOrder_id());

            if (!"payment".equalsIgnoreCase(paymentNotificationRequest.getStatus())) {
                Map<String, String> result = new HashMap<>();
                result.put("status", "error");
                result.put("message", "Payment status "
                        + paymentNotificationRequest.getStatus()
                        + " is not supported");
                return Mono.just(ResponseEntity.badRequest().body(result));
            }

            return paymentService.pay(paymentNotificationRequest.getOrder_id(),
                            paymentNotificationRequest.getPayment_amount(),
                            paymentNotificationRequest.getPayment_ref())
                    .flatMap(kafkaSenderService::send)
                    .map(x -> ResponseEntity.ok().build())
                    .onErrorResume(err -> {
                        log.error("Error processing payment notification : {}",err.getMessage());

                        Map<String, String> result = new HashMap<>();
                        result.put("status", "error");
                        result.put("message", err.getMessage());

                        if (VirtualAccountNotFoundException.class.isAssignableFrom(err.getClass())) {
                            return Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).body(result));
                        }

                        return Mono.just(ResponseEntity.badRequest().body(result));
                    });
        } catch (Exception err) {
            log.error("Error processing JSON {} | {}", err.getMessage(), request);
            return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        }
    }

    private boolean isValid(String signature, String request) {
        try {
            log.debug("Signature : {}", signature);
            log.debug("Request : {}", request);
            return signature.equalsIgnoreCase(apiKeyConfiguration.calculateSignature(request));
        } catch (Exception err) {
            log.error("Error validating signature : {}, signature : {}, request : {}",
                    err.getMessage(), signature, request);
            return false;
        }
    }

}
