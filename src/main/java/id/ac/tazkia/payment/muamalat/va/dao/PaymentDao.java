package id.ac.tazkia.payment.muamalat.va.dao;

import id.ac.tazkia.payment.muamalat.va.entity.Payment;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface PaymentDao extends ReactiveCrudRepository<Payment, String> {
    Mono<Long> countByIdVirtualAccount(String id);
}
