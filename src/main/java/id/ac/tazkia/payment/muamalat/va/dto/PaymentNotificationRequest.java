package id.ac.tazkia.payment.muamalat.va.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class PaymentNotificationRequest {

    @NotNull @NotEmpty
    private String order_id;

    @NotNull @NotEmpty
    private String payment_ref;
    private String platform;
    private String channel_id;

    @NotNull @Min(1)
    private BigDecimal payment_amount;
    private BigDecimal deposit_amount;

    @NotNull @Past
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime trx_date;
    private String status;
    private BigDecimal remaining_bill_amount;
}
