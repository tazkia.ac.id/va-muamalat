package id.ac.tazkia.payment.muamalat.va.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Payment {
    @Id private String id;
    private LocalDateTime transactionTime = LocalDateTime.now();

    @Transient
    private VirtualAccount virtualAccount;
    private String idVirtualAccount;
    private String clientReference;
    private BigDecimal amount;
    private PaymentStatus paymentStatus;
}
