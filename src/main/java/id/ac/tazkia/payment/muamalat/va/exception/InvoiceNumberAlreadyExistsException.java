package id.ac.tazkia.payment.muamalat.va.exception;

public class InvoiceNumberAlreadyExistsException extends PaymentServiceException {
    public InvoiceNumberAlreadyExistsException(String message) {
        super(message);
    }
}
