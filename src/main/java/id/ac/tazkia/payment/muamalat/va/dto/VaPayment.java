package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class VaPayment extends KafkaResponse {
    private String bankId;
    private String invoiceNumber;
    private String reference;
    private BigDecimal amount;
    private BigDecimal cumulativeAmount;
    private LocalDateTime paymentTime;
}
