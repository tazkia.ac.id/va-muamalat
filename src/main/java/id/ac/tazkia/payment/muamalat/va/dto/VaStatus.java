package id.ac.tazkia.payment.muamalat.va.dto;

public enum VaStatus {
    CREATE,
    UPDATE,
    DELETE,
    INQUIRY,
    SEDANG_PROSES,
    AKTIF,
    NONAKTIF,
    ERROR
}
