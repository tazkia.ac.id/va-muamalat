package id.ac.tazkia.payment.muamalat.va.command;


import id.ac.tazkia.payment.muamalat.va.dto.VaRequest;
import id.ac.tazkia.payment.muamalat.va.dto.VaResponse;
import id.ac.tazkia.payment.muamalat.va.dto.VaStatus;
import reactor.core.publisher.Mono;

public interface VaHandler {
    boolean supports(VaStatus status);
    Mono<VaResponse> process(VaRequest request);
}
