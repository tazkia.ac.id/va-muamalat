package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateVaRequest {
    private String order_id;
    private String va_no;
    private String customer_name;
    private String timestamp = LocalDateTime.now()
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    private List<CreateVaRequestDetail> details = new ArrayList<>();
}
