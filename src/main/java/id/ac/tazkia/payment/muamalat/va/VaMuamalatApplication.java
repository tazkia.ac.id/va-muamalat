package id.ac.tazkia.payment.muamalat.va;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaMuamalatApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaMuamalatApplication.class, args);
	}

}
