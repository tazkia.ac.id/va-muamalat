package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

@Data
public class CreateVaRequestDetail {
    private Long price;
    private Integer quantity;
    private String name;
}
