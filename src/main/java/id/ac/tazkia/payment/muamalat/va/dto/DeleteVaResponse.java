package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

@Data
public class DeleteVaResponse {
    private String status;
    private String message;
}
