package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VaResponse extends KafkaResponse {
    private VaStatus requestType;
    private VaRequestStatus requestStatus;
    private String invoiceNumber;
    private String name;
    private BigDecimal amount;
    private String bankId;
}
