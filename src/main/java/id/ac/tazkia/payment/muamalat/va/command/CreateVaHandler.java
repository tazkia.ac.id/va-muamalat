package id.ac.tazkia.payment.muamalat.va.command;

import id.ac.tazkia.payment.muamalat.va.dto.VaRequest;
import id.ac.tazkia.payment.muamalat.va.dto.VaRequestStatus;
import id.ac.tazkia.payment.muamalat.va.dto.VaResponse;
import id.ac.tazkia.payment.muamalat.va.dto.VaStatus;
import id.ac.tazkia.payment.muamalat.va.entity.VirtualAccount;
import id.ac.tazkia.payment.muamalat.va.helper.PaymentServiceConstants;
import id.ac.tazkia.payment.muamalat.va.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

@Component @Slf4j
public class CreateVaHandler implements VaHandler {

    @Autowired private PaymentService paymentService;

    @Override
    public boolean supports(VaStatus status) {
        return VaStatus.CREATE.equals(status);
    }

    @Override
    public Mono<VaResponse> process(VaRequest request) {
        log.info("[VA-REQUEST-CREATE] - Account No : {}, Account Name :{}, Amount : {}",
                request.getAccountNumber(), request.getName(), request.getAmount());
        VaResponse vaResponse = new VaResponse();
        BeanUtils.copyProperties(request, vaResponse);

        VirtualAccount va = new VirtualAccount();
        BeanUtils.copyProperties(request, va);
        va.setAccountName(request.getName());
        va.setAccountNumber(String.format("%-10s", request.getAccountNumber() ).replace(' ', '0'));

        if(StringUtils.hasText(request.getInvoiceType())) {
            va.setInvoiceType(PaymentServiceConstants.INVOICE_TYPE_PREFIX + request.getInvoiceType());
        } else {
            va.setInvoiceType(PaymentServiceConstants.INVOICE_TYPE_PREFIX + PaymentServiceConstants.INVOICE_TYPE_PREFIX_DEFAULT);
        }

        log.debug("[VA-REQUEST-CREATE] : {}",va);

        return paymentService.create(va)
                .map(v -> {
                    vaResponse.setRequestStatus(VaRequestStatus.SUCCESS);
                    vaResponse.setAccountNumber(va.getAccountNumber());
                    log.info("[VA-REQUEST-CREATE] - Success : {}-{}-{}", va.getAccountNumber(), va.getAccountName(), va.getAmount());
                    return vaResponse;
                })
                .onErrorResume(err -> {
                    vaResponse.setRequestStatus(VaRequestStatus.ERROR);
                    log.error("[VA-REQUEST-CREATE] - Error : {}", err.getMessage());
                    return Mono.just(vaResponse);
                })
                .cast(VaResponse.class);
    }
}
