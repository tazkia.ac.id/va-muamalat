package id.ac.tazkia.payment.muamalat.va.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidRequestException extends PaymentServiceException {
    public InvalidRequestException(String msg){
        super(msg);
    }
}