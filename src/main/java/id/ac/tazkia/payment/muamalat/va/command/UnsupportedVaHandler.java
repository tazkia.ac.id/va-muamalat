package id.ac.tazkia.payment.muamalat.va.command;

import id.ac.tazkia.payment.muamalat.va.dto.VaRequest;
import id.ac.tazkia.payment.muamalat.va.dto.VaResponse;
import id.ac.tazkia.payment.muamalat.va.dto.VaStatus;
import reactor.core.publisher.Mono;

public class UnsupportedVaHandler implements VaHandler {

    @Override
    public boolean supports(VaStatus status) {
        return true;
    }

    @Override
    public Mono<VaResponse> process(VaRequest request) {
        return Mono.error(new UnsupportedOperationException("Request Type "+request.getRequestType()+" is not supported"));
    }
}
