package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateVaResponse {

    private String status;
    private String message;
    private CreateVaResponseData data;

    @Data
    static class CreateVaResponseData {
        private String order_id;
        private String va_no;
        private String customer_name;
        private String timestamp;
        private Integer gross_amount;
        private List<CreateVaResponseDetail> details = new ArrayList<>();
    }
}
