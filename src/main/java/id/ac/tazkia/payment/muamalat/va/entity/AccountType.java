package id.ac.tazkia.payment.muamalat.va.entity;

public enum AccountType {
    OPEN,CLOSED,INSTALLMENT
}
