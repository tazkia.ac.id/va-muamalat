package id.ac.tazkia.payment.muamalat.va.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.payment.muamalat.va.dto.KafkaResponse;
import id.ac.tazkia.payment.muamalat.va.dto.VaPayment;
import id.ac.tazkia.payment.muamalat.va.dto.VaResponse;
import id.ac.tazkia.payment.muamalat.va.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Service @Slf4j
@Transactional
public class KafkaSenderService {

    @Value("${muamalat.client-id}") private String bankClientId;
    @Value("${kafka.topic.va.response}") private String kafkaTopicResponse;
    @Value("${kafka.topic.va.payment}") private String kafkaTopicPayment;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;

    public Mono<Void> send(VaResponse response) {
        return send(response, kafkaTopicResponse);
    }

    public Mono<Payment> send(Payment payment) {
        VaPayment vaPayment = new VaPayment();
        vaPayment.setBankId(bankClientId);
        vaPayment.setInvoiceNumber(payment.getVirtualAccount().getInvoiceNumber());
        vaPayment.setAccountNumber(payment.getVirtualAccount().getAccountNumber());
        vaPayment.setAmount(payment.getAmount());
        vaPayment.setCumulativeAmount(payment.getVirtualAccount().getCumulativePayment());
        vaPayment.setPaymentTime(payment.getTransactionTime());
        vaPayment.setReference(payment.getClientReference());
        return send(vaPayment, kafkaTopicPayment).thenReturn(payment);
    }

    public Mono<Void> send(KafkaResponse kafkaResponse, String topic) {
        return Mono.just(kafkaResponse)
            .flatMap(v -> Mono.fromCallable(() -> objectMapper.writeValueAsString(v)))
            .onErrorResume(err -> {
                log.warn("Error parsing VA Response json : {}", err.getMessage());
                return Mono.empty();
            }).map(msg -> {
                    if (VaPayment.class.isAssignableFrom(kafkaResponse.getClass())) {
                        log.info("[VA-PAYMENT] - [MESSAGE]: {}", msg);
                    } else if (VaResponse.class.isAssignableFrom(kafkaResponse.getClass())) {
                        log.info("[VA-RESPONSE] - [MESSAGE]: {}", msg);
                    } else {
                        return Mono.error(new UnsupportedOperationException(
                                kafkaResponse.getClass().getName() + " is not supported"));
                    }
                return kafkaTemplate.send(topic, msg);
            }).then();
    }
}
