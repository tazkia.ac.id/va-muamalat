package id.ac.tazkia.payment.muamalat.va.exception;

public class BankMuamalatException extends Exception {
    public BankMuamalatException(String message) {
        super(message);
    }
}
