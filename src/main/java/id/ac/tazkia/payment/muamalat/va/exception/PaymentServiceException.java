package id.ac.tazkia.payment.muamalat.va.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PaymentServiceException extends Exception {
    public PaymentServiceException(String msg){
        super(msg);
    }
}