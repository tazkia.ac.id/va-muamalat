package id.ac.tazkia.payment.muamalat.va.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.payment.muamalat.va.config.ApiKeyConfiguration;
import id.ac.tazkia.payment.muamalat.va.dto.*;
import id.ac.tazkia.payment.muamalat.va.entity.VirtualAccount;
import id.ac.tazkia.payment.muamalat.va.exception.BankMuamalatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service @Slf4j
public class BankMuamalatVaClientService {

    private static final String SIGNATURE_HEADER = "X-CLASSID-SIGNATURE";
    private static final String URL_GET_ALL_VA = "/transaction/list";
    private static final String URL_CREATE_VA = "/transaction/populate";
    private static final String URL_DELETE_VA = "/transaction/{id}";

    @Value("${muamalat.client-prefix}") private String clientPrefix;
    @Value("${muamalat.client-id}") private String clientId;

    @Autowired private WebClient webClient;
    @Autowired private ApiKeyConfiguration apiKeyConfiguration;
    @Autowired private ObjectMapper objectMapper;

    public Mono<VirtualAccount> createVa(VirtualAccount virtualAccount) {
        log.debug("Requesting create va for invoice {} to bank muamalat", virtualAccount.getInvoiceNumber());
        CreateVaRequest request = new CreateVaRequest();
        request.setOrder_id(virtualAccount.getInvoiceNumber());
        request.setVa_no(clientPrefix+clientId + virtualAccount.getAccountNumber());
        request.setCustomer_name(virtualAccount.getAccountName());

        CreateVaRequestDetail detail = new CreateVaRequestDetail();
        detail.setName(virtualAccount.getInvoiceType());
        detail.setPrice(virtualAccount.getAmount().longValue());
        detail.setQuantity(1);
        request.getDetails().add(detail);

        return createVa(request)
                .flatMap(response -> {
                    log.debug("Create va response from muamalat : {}", response.getStatus());
                    if (!"success".equalsIgnoreCase(response.getStatus())) {
                        return Mono.error(new BankMuamalatException("Error dari bank muamalat : " + response.getMessage()));
                    }
                    return Mono.just(virtualAccount);
                });
    }

    private Mono<CreateVaResponse> createVa(CreateVaRequest request) {
        return Mono.fromCallable(() -> objectMapper.writeValueAsString(request))
                .onErrorResume(err -> {
                    log.error(err.getMessage(), err);
                    return Mono.empty();
                })
                .flatMap(body -> webClient.post()
                            .uri(URL_CREATE_VA)
                            .header(SIGNATURE_HEADER,
                                    apiKeyConfiguration.calculateSignature(body))
                            .bodyValue(body)
                            .retrieve()
                            .bodyToMono(CreateVaResponse.class)
                );
    }

    public Mono<GetAllVaResponse> getAllVirtualAccounts() {
        return webClient.method(HttpMethod.GET)
                .uri(URL_GET_ALL_VA)
                .body(Mono.just(new GetAllVaRequest()), GetAllVaRequest.class)
                .retrieve()
                .bodyToMono(GetAllVaResponse.class);
    }

    public Mono<VirtualAccount> deleteVa(VirtualAccount virtualAccount) {
        return Mono.fromCallable(() -> objectMapper.writeValueAsString(new GetAllVaRequest()))
                .onErrorResume(err -> {
                    log.error(err.getMessage(), err);
                    return Mono.empty();
                })
                .flatMap(body -> webClient.method(HttpMethod.DELETE)
                    .uri(uriBuilder -> uriBuilder.path(URL_DELETE_VA).build(virtualAccount.getInvoiceNumber()))
                        .header(SIGNATURE_HEADER,
                                apiKeyConfiguration.calculateSignature(body))
                        .bodyValue(body)
                    .retrieve()
                    .bodyToMono(DeleteVaResponse.class)
                    .flatMap(response -> {
                        log.debug("Delete va response from muamalat : {}", response.getStatus());
                        if (!"success".equalsIgnoreCase(response.getStatus())) {
                            return Mono.error(new BankMuamalatException("Error dari bank muamalat : " + response.getMessage()));
                        }
                        return Mono.just(virtualAccount);
                    }));
    }
}
