package id.ac.tazkia.payment.muamalat.va.command;

import id.ac.tazkia.payment.muamalat.va.dto.VaStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component @Slf4j
public class VaHandlerFactory {

    @Autowired private Set<VaHandler> commands;
    private VaHandler defaultCommand = new UnsupportedVaHandler();

    public VaHandler getHandler(VaStatus status){
        log.debug("Getting handler for {}", status);
        for(VaHandler c : commands) {
            if(c.supports(status)) return c;
        }

        return defaultCommand;
    }
}
