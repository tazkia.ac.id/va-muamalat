package id.ac.tazkia.payment.muamalat.va.service;

import id.ac.tazkia.payment.muamalat.va.dao.PaymentDao;
import id.ac.tazkia.payment.muamalat.va.dao.VirtualAccountDao;
import id.ac.tazkia.payment.muamalat.va.entity.*;
import id.ac.tazkia.payment.muamalat.va.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Service @Transactional @Slf4j
public class PaymentService {

    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PaymentDao paymentDao;
    @Autowired private BankMuamalatVaClientService bankMuamalatVaClientService;

    public Flux<VirtualAccount> findByAccountNumber(String accountNumber) {
        if (!StringUtils.hasText(accountNumber)){
            return Flux.error(new VirtualAccountNotFoundException("Account number empty"));
        }
        return virtualAccountDao.findByAccountNumberAndAccountStatus(accountNumber, AccountStatus.ACTIVE);
    }

    public Mono<VirtualAccount> create(final VirtualAccount va) {
        log.debug("Creating virtual account {}", va.getAccountNumber());
        return Mono.just(va)
                .flatMapMany(v -> virtualAccountDao.findByAccountNumberAndAccountStatus(v.getAccountNumber(), AccountStatus.ACTIVE))
                .flatMap(x -> Flux.error(new VirtualAccountNumberAlreadyExistsException("VA number " + va.getAccountNumber() + " already exists")))
                .switchIfEmpty(virtualAccountDao.findByInvoiceNumber(va.getInvoiceNumber()))
                .flatMap(x -> Flux.error(new InvoiceNumberAlreadyExistsException("Invoice number " + va.getInvoiceNumber() + " already exists")))
                .switchIfEmpty(Mono.just(va)).single().cast(VirtualAccount.class)
                .map(v -> {
                    log.debug("Creating virtual account : {}", v);
                    return v;
                })
                .flatMap(bankMuamalatVaClientService::createVa)
                .flatMap(virtualAccountDao::save);
    }

    public Mono<VirtualAccount> update(VirtualAccount virtualAccount){
        return paymentDao.countByIdVirtualAccount(virtualAccount.getId())
            .handle((count, sink) -> {
                if(count > 0) {
                    sink.error(new VirtualAccountAlreadyPaidException("VA "+virtualAccount.getAccountNumber()+" already has payment in it"));
                    return;
                }
                sink.next(count);
            })
            .then(virtualAccountDao.save(virtualAccount));
    }

    public Mono<VirtualAccount> delete(String invoiceNumber) {
        log.debug("Delete virtual account with invoice number {}", invoiceNumber);
        return virtualAccountDao.findByInvoiceNumberAndAccountStatus(invoiceNumber, AccountStatus.ACTIVE)
                .switchIfEmpty(Mono.error(new VirtualAccountNotFoundException("VA with invoice number " + invoiceNumber + " not found")))
                .zipWhen(va -> paymentDao.countByIdVirtualAccount(va.getId()))
                .handle((vaCount, sink) -> {
                    VirtualAccount va = vaCount.getT1();
                    Long numPayment = vaCount.getT2();
                    if(numPayment > 0) {
                        sink.error(new VirtualAccountAlreadyPaidException("Invoice "+invoiceNumber+" already has payment in it"));
                        return;
                    }
                    sink.next(va);
                }).cast(VirtualAccount.class)
                .flatMap(bankMuamalatVaClientService::deleteVa)
                .flatMap(va -> virtualAccountDao.delete(va).thenReturn(va));
    }

    public Mono<Payment> pay(String invoiceNumber, BigDecimal amount, String reference) {
        if (!StringUtils.hasText(reference)) {
            return Mono.error(new InvalidRequestException("Payment Reference missing"));
        }
        return virtualAccountDao.findByInvoiceNumberAndAccountStatus(invoiceNumber, AccountStatus.ACTIVE)
                .switchIfEmpty(Mono.error(new VirtualAccountNotFoundException("Invoice number "+invoiceNumber+ " not found")))
                .handle((va, sink) -> {
                    if(AccountType.CLOSED.equals(va.getAccountType())) {
                        if (va.getAmount().compareTo(amount) != 0) {
                            sink.error(new PaymentAmountMismatchException("Amount sent " + amount + ", should be " + va.getAmount()));
                            return;
                        }
                    } else if (AccountType.OPEN.equals(va.getAccountType())) {
                        if (va.getAmount().compareTo(amount) > 0) {
                            sink.error(new PaymentAmountMismatchException("Amount sent " + amount + ", should be greater than " + va.getAmount()));
                            return;
                        }
                    } else if (AccountType.INSTALLMENT.equals(va.getAccountType())) {
                        if (va.getAmount().compareTo(amount.add(va.getCumulativePayment())) < 0) {
                            sink.error(new PaymentAmountMismatchException("Amount sent " + amount + ", should be less than " + va.getAmount().subtract(va.getCumulativePayment())));
                            return;
                        }
                    }
                    sink.next(va);
                }).cast(VirtualAccount.class)
                .flatMap(va -> {
                    va.setCumulativePayment(va.getCumulativePayment().add(amount));
                    if (va.getCumulativePayment().compareTo(va.getAmount()) == 0) {
                        va.setAccountStatus(AccountStatus.INACTIVE);
                        va.setPaymentStatus(PaymentStatus.FULL);
                    } else {
                        va.setPaymentStatus(PaymentStatus.PARTIAL);
                    }
                    return virtualAccountDao.save(va);
                })
                .flatMap(va -> {
                    Payment payment = new Payment();
                    payment.setVirtualAccount(va);
                    payment.setIdVirtualAccount(va.getId());
                    payment.setAmount(amount);
                    payment.setClientReference(reference);

                    if (va.getCumulativePayment().compareTo(va.getAmount()) == 0) {
                        payment.setPaymentStatus(PaymentStatus.FULL);
                    } else {
                        payment.setPaymentStatus(PaymentStatus.PARTIAL);
                    }

                    return paymentDao.save(payment);
                });
    }
}
