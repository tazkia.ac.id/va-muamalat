package id.ac.tazkia.payment.muamalat.va.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class VirtualAccountAlreadyPaidException extends PaymentServiceException {
    public VirtualAccountAlreadyPaidException(String msg){
        super(msg);
    }
}