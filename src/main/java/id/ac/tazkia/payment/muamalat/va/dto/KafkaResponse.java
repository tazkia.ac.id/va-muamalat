package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

@Data
public class KafkaResponse {
    private String accountNumber;
}
