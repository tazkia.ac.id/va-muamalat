package id.ac.tazkia.payment.muamalat.va.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.payment.muamalat.va.command.VaHandlerFactory;
import id.ac.tazkia.payment.muamalat.va.dto.VaRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service @Slf4j
public class KafkaListenerService {

    @Value("${muamalat.bank-id}") private String bankId;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private VaHandlerFactory vaHandlerFactory;
    @Autowired private KafkaSenderService kafkaSenderService;

    @KafkaListener(topics = "${kafka.topic.va.request}", autoStartup = "${kafka.listen.auto.start:false}")
    public void receiveVaRequest(String message) {
        log.info("[VA-REQUEST] - [MESSAGE]: {}", message);
        Mono.just(message)
                .flatMap(m -> Mono.fromCallable(() -> objectMapper.readValue(m, VaRequest.class)))
                .onErrorResume(err -> {
                    log.warn("Error parsing VA Request json : {}", err.getMessage());
                    return Mono.empty();
                })
                .filter(v -> {
                    Boolean bankIdMatches = bankId.equals(v.getBankId());
                    if(!bankIdMatches) {
                        log.info("[VA-REQUEST] - [SKIPPED] : Bank ID {}", v.getBankId());
                    }
                    return bankIdMatches;
                })
                .flatMap(vaRequest -> vaHandlerFactory
                            .getHandler(vaRequest.getRequestType())
                            .process(vaRequest))
                .flatMap(kafkaSenderService::send)
                .block();
    }
}
