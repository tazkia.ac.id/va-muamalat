package id.ac.tazkia.payment.muamalat.va.entity;

public enum PaymentStatus {
    UNPAID,PARTIAL,FULL
}
