package id.ac.tazkia.payment.muamalat.va.exception;

public class VirtualAccountNumberAlreadyExistsException extends PaymentServiceException {
    public VirtualAccountNumberAlreadyExistsException(String message) {
        super(message);
    }
}
