package id.ac.tazkia.payment.muamalat.va.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
public class ApiKeyConfiguration {
    public static final String HMAC_SHA_256 = "HmacSHA256";
    @Value("${api.secret}") private String apiSecret;

    private Mac hmacSHA256;

    @PostConstruct
    public void initialize() throws NoSuchAlgorithmException, InvalidKeyException {
        hmacSHA256 = Mac.getInstance(HMAC_SHA_256);
        SecretKeySpec secretKeySpec = new SecretKeySpec(apiSecret.getBytes(), HMAC_SHA_256);
        hmacSHA256.init(secretKeySpec);
    }

    public String calculateSignature(String textToSign) {
        return new String(Hex.encode(hmacSHA256.doFinal(textToSign.getBytes())));
    }
}
