package id.ac.tazkia.payment.muamalat.va.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class VirtualAccount {

    @Id private String id;
    private String invoiceNumber;
    private String invoiceType;
    private String accountNumber;
    private String accountName;
    private String email;
    private String phone;
    private String description;
    private AccountType accountType = AccountType.CLOSED;

    private BigDecimal amount;
    private BigDecimal cumulativePayment = BigDecimal.ZERO;

    private LocalDate expireDate = LocalDate.now().plusMonths(6);
    private LocalDateTime createTime = LocalDateTime.now();
    private AccountStatus accountStatus = AccountStatus.ACTIVE;
    private PaymentStatus paymentStatus = PaymentStatus.UNPAID;

    public BigDecimal effectiveAmount(){
        if(AccountType.INSTALLMENT.equals(accountType)){
            return amount.subtract(cumulativePayment);
        }

        return amount;
    }
}
