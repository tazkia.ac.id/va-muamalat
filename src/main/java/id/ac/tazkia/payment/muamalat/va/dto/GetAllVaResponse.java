package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GetAllVaResponse {
    private String status;
    private Integer current_page;
    private Integer last_page;
    private Integer per_page;
    private String first_page_url;
    private String next_page_url;
    private String prev_page_url;
    private Integer from;
    private Integer to;
    private Integer total;
    private List<CreateVaResponse> data = new ArrayList<>();
}
