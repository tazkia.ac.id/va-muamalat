package id.ac.tazkia.payment.muamalat.va.dto;

import lombok.Data;

@Data
public class CreateVaResponseDetail {
    private Integer price;
    private Integer quantity;
    private Integer total;
    private String name;
}
