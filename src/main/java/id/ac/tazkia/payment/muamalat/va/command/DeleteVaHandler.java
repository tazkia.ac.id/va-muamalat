package id.ac.tazkia.payment.muamalat.va.command;

import id.ac.tazkia.payment.muamalat.va.dto.VaRequest;
import id.ac.tazkia.payment.muamalat.va.dto.VaRequestStatus;
import id.ac.tazkia.payment.muamalat.va.dto.VaResponse;
import id.ac.tazkia.payment.muamalat.va.dto.VaStatus;
import id.ac.tazkia.payment.muamalat.va.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component @Slf4j
public class DeleteVaHandler implements VaHandler {
    @Autowired
    private PaymentService paymentService;

    @Override
    public boolean supports(VaStatus status) {
        return VaStatus.DELETE.equals(status);
    }

    @Override
    public Mono<VaResponse> process(final VaRequest request) {
        return Mono.just(request)
            .flatMap(r -> {
                log.info("[VA-REQUEST-DELETE] - Account No : {}, Invoice Number : {}, Account Name :{}, Amount : {}",
                        request.getAccountNumber(),
                        request.getInvoiceNumber(),
                        request.getName(), request.getAmount());
                return paymentService.delete(request.getInvoiceNumber());
            }).map(va -> {
                log.info("[VA-REQUEST-DELETE] - Success : {}-{}-{}",
                    va.getAccountNumber(), va.getAccountName(), va.getAmount());
                VaResponse vaResponse = new VaResponse();
                vaResponse.setRequestStatus(VaRequestStatus.SUCCESS);
                BeanUtils.copyProperties(request, vaResponse);
                vaResponse.setAccountNumber(va.getAccountNumber());
                vaResponse.setName(va.getAccountName());
                vaResponse.setAmount(va.getAmount());
                return vaResponse;
            }).onErrorResume(e -> {
                log.error(e.getMessage(), e);
                log.info("[VA-REQUEST-DELETE] - Error : {}-{}", request.getAccountNumber(), e.getMessage());
                VaResponse vaResponse = new VaResponse();
                vaResponse.setRequestStatus(VaRequestStatus.ERROR);
                BeanUtils.copyProperties(request, vaResponse);
                return Mono.just(vaResponse);
            });
    }
}
